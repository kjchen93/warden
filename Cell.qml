import QtQuick 2.0
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.0
import QtQuick.Window 2.15

Rectangle {
        id: item
        width: bluePrint.width/6
        height: 120
        color: "#001484"
        border.color: "white"
        border.width: 2
        property bool selected: false
        property bool open: false
        Item
        {
            id: door
            width: parent.width*2/3
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            Rectangle
            {
                anchors.right: parent.right
                anchors.verticalCenter: parent.top
                width: parent.width/2
                height: 20
            }

        }
        RotationAnimation
        {
            id:open_door
            target: door
            from: 0
            to: 90
            duration: 300
        }

        RotationAnimation
        {
            id:close_door
            target: door
            from: 90
            to: 0
            duration: 300
        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.family: "Helvetica"
            font.pointSize: 24
            color: "white"
            text: {
                var text = "Cell: " + index
                return text;
            }

        }


        MouseArea {
            id:highlightArea
            anchors.fill: parent
            onClicked: {
                if (!selected)
                {
                    animationHighlight.start()

                }
                else
                {
                    animationDelight.start()
                }
                selected=!selected
            }
        }

        SequentialAnimation on color {
            id: animationHighlight
            running: false

            ColorAnimation { to: "#2ECC71"; duration: 50 }
            ColorAnimation { to: "#3498DB"; duration: 100 }
        }

        SequentialAnimation on color {
            id: animationDelight
            running: false

            ColorAnimation { to: "#3498DB"; duration: 50 }
            ColorAnimation { to: "#001484"; duration: 100 }
        }

        Connections {
            id: cells_connections
            target: appwindow
            function onDoorOpen ()
            {
                if(selected && !open)
                {
                    open_door.start()
                    open=true
                }
            }
            function onDoorClose()
            {
                if(selected && open)
                {
                    close_door.start()
                    open=false
                }
            }
            function onAllDoorsOpen()
            {
                if(!open)
                {
                    open_door.start()
                    open=true
                }
            }
            function onAllDoorsClose()
            {
                if(open)
                {
                    close_door.start()
                    open=false
                }
            }
        }
}
