import QtQuick 2.0
import QtTest 1.2
Item {
    Cell {
        id: cellTest
    }
    TestCase {
        name: "CellColor"
        when: windowShown

        function test_CellColor(){
            var cellColor =  "#001484"
            compare(cellTest.color, cellColor, "Cell color:" + cellTest.color)
        }
    }

}
