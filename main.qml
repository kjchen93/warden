import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts
import QtQuick.Controls

ApplicationWindow {
    id: appwindow
    width: 1280
    height: 720
    visible: true
    title: qsTr("Warden")
    color: "black"
    signal doorOpen()
    signal doorClose()
    signal allDoorsOpen()
    signal allDoorsClose()
    Component.onCompleted: {
        x = Screen.width / 2 - width / 2
        y = Screen.height / 2 - height / 2
    }
    Shortcut {
        sequence: "Ctrl+Q"
        onActivated: Qt.quit()
    }
    header: ToolBar {
        RowLayout {
            id: headerRowLayout
            anchors.fill: parent
            spacing: 0

            ToolButton {
                //icon.name: "grid"
            }
            //            ToolButton {
            //                icon.name: "settings"
            //            }
            //            ToolButton {
            //                icon.name: "filter"
            //            }
            //            ToolButton {
            //                icon.name: "message"
            //            }
            //            ToolButton {
            //                icon.name: "music"
            //            }
            //            ToolButton {
            //                icon.name: "cloud"
            //            }
            //            ToolButton {
            //                icon.name: "bluetooth"
            //            }
            //            ToolButton {
            //                icon.name: "cart"
            //            }
            //            ToolButton {
            //                icon.name: "amazon"
            //            }

            Item {
                Layout.fillWidth: true
            }
            ToolButton {
                onClicked: Qt.quit()
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        spacing:0
        Rectangle {
            id:bluePrint
            Layout.alignment: Qt.AlignHCenter
            width: appwindow.width*5/6
            height: appwindow.height*2/3
            color: "#001484"
            border.color: "white"
            border.width: 2

            DelegateModel {
                id: cellModel
                model: 6
                delegate: Cell {}
            }


            ListView {
                id: cell_row
                anchors.fill: parent
                interactive: false
                model: cellModel
                orientation: ListView.Horizontal
            }

            Image {
                id: fire_symbol
                anchors.centerIn: parent
                source: "qrc:/icons/fire.png"
                opacity: 0
            }
            Image {
                id: escape_symbol
                anchors.centerIn: parent
                source: "qrc:/icons/escape.png"
                opacity: 0
            }
            SequentialAnimation on color
            {
                id: alarm_animation
                loops: Animation.Infinite
                running: false
                ColorAnimation {

                    from: "#001484"
                    to: "red"
                    duration: 1000
                }
                ColorAnimation {

                    from: "red"
                    to: "#001484"
                    duration: 1000
                }
            }
        }

        Rectangle {
            id: control_board
            width: appwindow.width
            height: appwindow.height*1/3
            color: "#343434"
            RowLayout
            {
                anchors.fill: parent
                Button{
                    id: open_btn
                    Layout.fillWidth: parent
                    text: "Open"
                    onClicked:
                    {
                        appwindow.doorOpen()
                        console.log(cell_row.count)

                    }
                }
                Button{
                    id: close_btn
                    Layout.fillWidth: parent
                    text: "Close"
                    onClicked:
                    {

                        appwindow.doorClose()
                    }
                }
                Button{
                    id: escape_alarm_btn
                    Layout.fillWidth: parent
                    text: "Escape Alarm"
                    property bool running: false
                    onClicked:
                    {
                        if(!running)
                        {
                            appwindow.allDoorsClose()
                            escape_symbol.opacity=1
                            alarm_animation.start()

                            //Disable other buttons
                            fire_alarm_btn.enabled=false
                            open_btn.enabled=false
                            close_btn.enabled=false
                            running=true
                        }
                        else
                        {
                            fire_alarm_btn.enabled=true
                            open_btn.enabled=true
                            close_btn.enabled=true
                            alarm_animation.stop()
                            bluePrint.color="#001484"
                            escape_symbol.opacity=0
                            running=false
                        }
                    }
                }
                Button{
                    id: fire_alarm_btn
                    Layout.fillWidth: parent
                    text: "Fire Alarm"
                    property bool running: false
                    onClicked:
                    {
                        if(!running)
                        {


                            //Play animations
                            alarm_animation.start()
                            appwindow.allDoorsOpen()
                            fire_symbol.opacity=1

                            //Disable othe buttons
                            escape_alarm_btn.enabled=false
                            open_btn.enabled=false
                            close_btn.enabled=false

                            running=true
                        }
                        else
                        {
                            appwindow.allDoorsClose()
                            alarm_animation.stop()
                            fire_symbol.opacity=0
                            bluePrint.color="#001484"
                            escape_alarm_btn.enabled=true
                            open_btn.enabled=true
                            close_btn.enabled=true
                            running=false
                        }
                    }
                }
            }
        }
    }

}
