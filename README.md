# Warden

## Description
Warden is a proof-of-concept of a fururistic guardless prison application created in Qt. The entire operation of the prison is controlled by the warden of the prison. Everything from closing cell gates, cooking meals for inmates and advanced crowd control measurements such as tear gas and electrical shocking devices can all be controlled from the wardens controlcenter. Our first design of prototype has taken it's form as the images shown below.

At the current state, the warden can select one or multiple cell and an animation will highlight to confirm the selected cell(s). Once the cells are selected the warden can choose to open or close the specified cell.
![](./imgs/select_open.jpg)

The below image shows an example of use of the escape alarm. In cases where inmates tries to escape or fight, the escape alarm immedietly puts the prison in lockdown.

![](./imgs/firealarm.jpg)

## Unit Testsd
The below image shows the results of a unit test, verifying the color of the blueprint is **#001484** in hex code.

![](./imgs/test_image.jpg)


## Authors
[Ole Kjepso](gitlab.com/olemikole)
[Kai Chen](gitlab.com/kjchen93)
